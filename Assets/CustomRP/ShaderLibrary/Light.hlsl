#ifndef CUSTOM_LIGHT_INCLUDED
#define CUSTOM_LIGHT_INCLUDED

#define MAX_LIGHT_COUNT 4

CBUFFER_START(_CustomLight)
	//float3 _DirectionalLightColor;
	//float3 _DirectionalLightDirection;
	int _DirectionalLightCount;
	float4 _DirectionalLightColors[MAX_LIGHT_COUNT];
	float4 _DirectionalLightDirections[MAX_LIGHT_COUNT];
	float4 _DirectionalLightShadowData[MAX_LIGHT_COUNT];
CBUFFER_END

struct Light 
{
	float3 color;
	float3 direction;
	float attenuation;
};
int GetDirectionalLightCount()
{
	return _DirectionalLightCount;
}

DirectionalShadowData GetDirectionalShadowData(int lightIndex,ShadowData shadowData)
{
	DirectionalShadowData data;
	data.strength = _DirectionalLightShadowData[lightIndex].x;// * shadowData.strength ;
	data.tileIndex = _DirectionalLightShadowData[lightIndex].y + shadowData.cascadeIndex;
	data.normalBias = _DirectionalLightShadowData[lightIndex].z;
	data.shadowMaskChannel = _DirectionalLightShadowData[lightIndex].w;
	return data;
}

Light GetDirectionalLight(int index,Surface surface,ShadowData shadowData)
{
	Light light;
	light.color =_DirectionalLightColors[index].rgb;
	light.direction = _DirectionalLightDirections[index].xyz;
	DirectionalShadowData dirShadowData = GetDirectionalShadowData(index,shadowData);
	light.attenuation  = GetDirectionalShadowAttenuation(dirShadowData,shadowData,surface);
//	light.attenuation = shadowData.cascadeIndex * 0.25*0.5;
	return light;
}


#endif