#ifndef CUSTOM_SHADOWS_INCLUDED
#define CUSTOM_SHADOWS_INCLUDED
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Shadow/ShadowSamplingTent.hlsl"

#if defined(_DIRECTIONAL_PCF3)
	#define DIRECTIONAL_FILTER_SAMPLES 4
	#define DIRECTIONAL_FILTER_SETUP SampleShadow_ComputeSamples_Tent_3x3
#elif defined(_DIRECTIONAL_PCF5)
	#define DIRECTIONAL_FILTER_SAMPLES 9
	#define DIRECTIONAL_FILTER_SETUP SampleShadow_ComputeSamples_Tent_5x5
#elif defined(_DIRECTIONAL_PCF7)
	#define DIRECTIONAL_FILTER_SAMPLES 16
	#define DIRECTIONAL_FILTER_SETUP SampleShadow_ComputeSamples_Tent_7x7
#endif


#define MAX_SHADOWS_LIGHT_COUNT 4
#define MAX_CASCADE_COUNT 4


TEXTURE2D_SHADOW(_DirectionalShadowAtlas);

#define SHADOW_SAMPLER sampler_linear_clamp_compare

//SAMPLER_CMP(sampler_DirectionalShadowAtlas);
SAMPLER_CMP(SHADOW_SAMPLER);

CBUFFER_START(_CustomShadows)	
	int _CascadeCount;
	float4 _CascadeCullingSpheres[MAX_CASCADE_COUNT];
	float4x4 _DirectionalShadowMatrices[MAX_SHADOWS_LIGHT_COUNT * MAX_CASCADE_COUNT];
//	float _ShadowDistance;
	float4 _ShadowDistanceFade;
	float4 _CascadeData[MAX_CASCADE_COUNT];
	float4 _ShadowAtlasSize;
CBUFFER_END



struct DirectionalShadowData 
{
	float strength;
	int tileIndex;	
	float normalBias;
	int shadowMaskChannel;
};

struct ShadowMask {
	bool distance;
	bool always;
	float4 shadows;
};

struct ShadowData
{
	int cascadeIndex;
	float strength;
	ShadowMask shadowMask;
};



float SampleDirectionalShadowAtlas(float3 positionSTS)
{
	return SAMPLE_TEXTURE2D_SHADOW(_DirectionalShadowAtlas,SHADOW_SAMPLER,positionSTS);
}

float FilterDirectionalShadow(float3 positionSTS)
{
	#if defined(DIRECTIONAL_FILTER_SETUP)
		real weights[DIRECTIONAL_FILTER_SAMPLES];
		real2 positions[DIRECTIONAL_FILTER_SAMPLES];
		float4 size = _ShadowAtlasSize.yyxx;
		DIRECTIONAL_FILTER_SETUP(size,positionSTS.xy,weights,positions);
		float shadow = 0;
		for(int i=0; i < DIRECTIONAL_FILTER_SAMPLES; i++ )
		{
			shadow += weights[i] * SampleDirectionalShadowAtlas(float3(positions[i].xy,positionSTS.z));
		}
		return shadow; 
	#else 
		return SampleDirectionalShadowAtlas(positionSTS);
	#endif
}
float GetCascadeShadow(DirectionalShadowData data,ShadowData global, Surface surface)
{
	float3 normalBias = surface.interpolatedNormal * data.normalBias * _CascadeData[global.cascadeIndex].y;
	float3 positionSTS = mul( _DirectionalShadowMatrices[data.tileIndex],float4(surface.position + normalBias,1.0) ).xyz;

	//float shadow = SampleDirectionalShadowAtlas(positionSTS);
	float shadow = FilterDirectionalShadow(positionSTS);

	return shadow;
	//return 1.0;

}
float GetBakedShadow(ShadowMask mask,int channel)
{
	if(mask.distance || mask.always)
	{
		if( channel >= 0 )
		{
			return mask.shadows[channel];
		}
		
	}
	return 1.0;
}

float GetBakedShadow2(ShadowMask mask,float strength, int channel)
{
	if(mask.distance || mask.always)
	{
		return lerp(1.0,GetBakedShadow(mask,channel),strength);
	}
	return 1.0;
}

float MixBakedAndRealtimeShadows(ShadowData global, float shadow, float strength,int shadowMaskChannel)
{
	float baked = GetBakedShadow(global.shadowMask,shadowMaskChannel);
	if(global.shadowMask.always)
	{
		shadow = lerp(1.0,shadow,global.strength);

		shadow = min(baked,shadow);//这里我以为是max，但其实不是，目的是取两者都有的值
		return lerp(1.0,shadow,strength);
		//shadow = baked;
	}
	else if(global.shadowMask.distance)
	{
		shadow = lerp(baked,shadow,global.strength);//根据阴影距离强度，取两边值（烘焙阴影或实时阴影）
		return lerp(1.0,shadow,strength);
		//shadow = baked;
	} 
	
	return lerp(1.0,shadow,strength * global.strength);
}

float GetDirectionalShadowAttenuation(DirectionalShadowData dirShadowData,ShadowData global, Surface surface)
{
	if(dirShadowData.strength * global.strength <= 0.0)
	{
		return GetBakedShadow2(global.shadowMask,abs(dirShadowData.strength),dirShadowData.shadowMaskChannel);
	}
	
	float shadow = GetCascadeShadow(dirShadowData,global,surface);

	return MixBakedAndRealtimeShadows(global,shadow,dirShadowData.strength,dirShadowData.shadowMaskChannel);
	//return 1.0;

}
float FadeShadowStrength(float depth, float distance,float fade)
{
	return saturate( (1.0 - depth *  distance ) * fade );
}

float FadeShadowStrength2(float distance, float scale,float fade)
{
	return saturate( (1.0 - distance *  scale ) * fade );
}

ShadowData GetShadowData(Surface surface)
{
	ShadowData data;
	//data.strength = surface.depth < _ShadowDistance ? 1.0 : 0.0;
	data.strength = FadeShadowStrength(surface.depth,_ShadowDistanceFade.x,_ShadowDistanceFade.y);
	//data.strength = 1.0;
	int i;
	for (i=0;i < _CascadeCount;i++)
	{
		float4 sphere = _CascadeCullingSpheres[i];
		float distanceSqr = DistanceSquared(surface.position,sphere.xyz);
		if(distanceSqr < sphere.w)
		{
			if( i == _CascadeCount - 1)
			{
				data.strength *= FadeShadowStrength2(distanceSqr,_CascadeData[i].x, _ShadowDistanceFade.z);
			}
			break;	
		}
	}
	if( i == _CascadeCount)
	{
		data.strength = 0.0;
	}
	data.cascadeIndex = i;
	data.shadowMask.distance = false;
	data.shadowMask.always = false;
	data.shadowMask.shadows = 1.0;
	return data;
}
#endif