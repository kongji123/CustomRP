#ifndef CUSTOM_BRDF_INCLUDED
#define CUSTOM_BRDF_INCLUDED

struct BRDF 
{
	float3 diffuse;
	float3 specular;
	float  roughness;
	float	perceptualRoughness;
	float  fresnel;
};


#define MIN_REFLECTIVITY 0.04

float GetDiffuseReflectivity(float metallic)
{
	float range = 1 - MIN_REFLECTIVITY;
	float diffuseReflectivity = range - range * metallic;
	return diffuseReflectivity;
}


BRDF GetBRDF(Surface surface)
{
	BRDF brdf;
	float diffuseReflectivity = GetDiffuseReflectivity(surface.metallic);
	brdf.diffuse = surface.color * diffuseReflectivity ;
	#if defined(_DIFFUSE_PREMULTIPLY_ALPHA)
		brdf.diffuse *= surface.alpha;
	#endif
	//brdf.specular = surface.color - brdf.diffuse;
	brdf.specular =lerp(MIN_REFLECTIVITY,surface.color,surface.metallic);
	brdf.perceptualRoughness = PerceptualSmoothnessToPerceptualRoughness(surface.smoothness);	
	brdf.roughness = PerceptualRoughnessToRoughness(brdf.perceptualRoughness);
	brdf.fresnel = saturate(surface.smoothness +  MIN_REFLECTIVITY);
	return brdf;
}
#endif