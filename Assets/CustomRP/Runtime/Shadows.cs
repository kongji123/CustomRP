using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public class Shadows 
{
    const string bufferName = "Shadows";
    CommandBuffer buffer = new CommandBuffer
    {
        name = bufferName
    };

    ScriptableRenderContext context;

    CullingResults cullingResults;
    ShadowSettings settings;
    
    const int maxShadowedDirLightCount = 4, maxCascades = 4;
    static string[] directionalFilterKeywords =
    {
        "_DIRECTIONAL_PCF3",
        "_DIRECTIONAL_PCF5",
        "_DIRECTIONAL_PCF7",
    };
    struct ShadowedDirectionalLight
    {
        public int visibleLightIndex;
        public float slopeScaleBias;
        public float nearPlaneOffset;
    }
    ShadowedDirectionalLight[] shadowedDirctionalLights = new ShadowedDirectionalLight[maxShadowedDirLightCount];

    int shadowedDirectionalLightCount;

    static int dirShadowAtlasId = Shader.PropertyToID("_DirectionalShadowAtlas");
    static int dirShadowMatricesId = Shader.PropertyToID("_DirectionalShadowMatrices");
    static int cascadeCountId = Shader.PropertyToID("_CascadeCount");
    static int cascadeCullingSpheresId = Shader.PropertyToID("_CascadeCullingSpheres");
    //static int shadowDistanceId = Shader.PropertyToID("_ShadowDistance");
    static int shadowDistanceFadeId = Shader.PropertyToID("_ShadowDistanceFade");
    static int cascadeDataId = Shader.PropertyToID("_CascadeData");

    static int shadowAtlasSizeId = Shader.PropertyToID("_ShadowAtlasSize");

    static Matrix4x4[] dirShadowMatrices = new Matrix4x4[maxShadowedDirLightCount * maxCascades];
    static Vector4[] cascadeCullingSpheres = new Vector4[maxCascades];
    static Vector4[] cascadeData = new Vector4[maxCascades];


    static string[] shadowMaskKeywords =
    {
        "_SHADOW_MASK_ALWAYS",
        "_SHADOW_MASK_DISTANCE",
        
    };

    bool useShadowMask;
   


    public void Setup(ScriptableRenderContext context,CullingResults cullingResults,
        ShadowSettings shadowSettings)
    {
        this.context = context;
        this.cullingResults = cullingResults;
        this.settings = shadowSettings;
        shadowedDirectionalLightCount = 0;
        useShadowMask = false;

    }

    public Vector4 ReserveDirectionalShadows(Light light ,int visibleLightIndex)
    {
        if(shadowedDirectionalLightCount < maxShadowedDirLightCount&&
            light.shadows != LightShadows.None && light.shadowStrength > 0f
            //&& cullingResults.GetShadowCasterBounds(visibleLightIndex,out Bounds b) 
            )
        {
            float maskChannel = -1;
            LightBakingOutput lightBaking = light.bakingOutput;
            if(lightBaking.lightmapBakeType == LightmapBakeType.Mixed
                && lightBaking.mixedLightingMode == MixedLightingMode.Shadowmask)
            {
                useShadowMask = true;
                maskChannel = lightBaking.occlusionMaskChannel;
            }

            //实时阴影投射器
            if(cullingResults.GetShadowCasterBounds(visibleLightIndex, out Bounds b))
            {

            }
            else
            {
                return new Vector4(-light.shadowStrength, 0f, 0f,maskChannel);
            }
            shadowedDirctionalLights[shadowedDirectionalLightCount] = new ShadowedDirectionalLight
            {
                visibleLightIndex = visibleLightIndex,
                slopeScaleBias = light.shadowBias,
                nearPlaneOffset = light.shadowNearPlane,
            };
            Vector3 ret = new Vector4(light.shadowStrength,
                shadowedDirectionalLightCount *  this.settings.directional.cascadeCount,
                light.shadowNormalBias,
                maskChannel);
            shadowedDirectionalLightCount++;
            return ret;
        }
        return new Vector4(0f,0f,0f,-1f);
    }

    void ExecuteBuffer()
    {
        context.ExecuteCommandBuffer(buffer);
        buffer.Clear();
    }

    public void Render()
    {
        if(shadowedDirectionalLightCount > 0 )
        {
            RenderDirectionalShadows();
        }
        else
        {
            buffer.GetTemporaryRT(dirShadowAtlasId, 1, 1, 32, FilterMode.Bilinear, RenderTextureFormat.Shadowmap);//生成RT避免过度释放RT
        }

        buffer.BeginSample(bufferName);
        int index = (QualitySettings.shadowmaskMode == ShadowmaskMode.Shadowmask ? 0 : 1);
        SetKeywords(shadowMaskKeywords, useShadowMask? index : -1 );
        buffer.EndSample(bufferName);
        ExecuteBuffer();
    }

    void RenderDirectionalShadows()
    {
        int atlasSize = (int)settings.directional.atlasSize;
       
        
        buffer.GetTemporaryRT(dirShadowAtlasId, atlasSize, atlasSize,
            32,FilterMode.Bilinear,RenderTextureFormat.Shadowmap);
        buffer.SetRenderTarget(dirShadowAtlasId, RenderBufferLoadAction.DontCare,
            RenderBufferStoreAction.Store);
        buffer.ClearRenderTarget(true, false, Color.clear);
        ExecuteBuffer();


        buffer.BeginSample(bufferName);
        ExecuteBuffer();

        int tiles = shadowedDirectionalLightCount * this.settings.directional.cascadeCount;
        int splitCount = tiles <= 1 ? 1 : tiles<= 4 ? 2 :4 ;
        int tileSize = atlasSize / splitCount;

        for (int i=0; i< shadowedDirectionalLightCount; i++)
        {          
            RenderDirectionalShadow(i, splitCount, tileSize);
        }

        
        buffer.SetGlobalInt(cascadeCountId, settings.directional.cascadeCount);
        buffer.SetGlobalVectorArray(cascadeCullingSpheresId, cascadeCullingSpheres);
        buffer.SetGlobalMatrixArray(dirShadowMatricesId, dirShadowMatrices);
        // buffer.SetGlobalFloat(shadowDistanceId, settings.maxDistance);
        float f = 1f - settings.directional.cascadeFade;
        buffer.SetGlobalVector(shadowDistanceFadeId, new Vector4(1f / settings.maxDistance, 1f / settings.distanceFade,
            1f / (1f - f * f)
            )); ;
        buffer.SetGlobalVectorArray(cascadeDataId, cascadeData);
        buffer.SetGlobalVector(shadowAtlasSizeId, new Vector4(atlasSize, 1f / atlasSize));

        SetDirectionalKeywords();
        buffer.EndSample(bufferName);
        ExecuteBuffer();
    }

    void SetCascadeData(int index,Vector4 cullingSphere,float tileSize)
    {       
        cullingSphere.w *= cullingSphere.w;
        cascadeCullingSpheres[index] = cullingSphere;
        // cascadeData[index].x = 1f / cullingSphere.w;
        float texelSize = 2f * cullingSphere.w / tileSize;
        float filterSize = texelSize * ((float)settings.directional.filter + 1f);
        cascadeData[index] = new Vector4(1f / cullingSphere.w, filterSize * 1.4142f);
    }
    void RenderDirectionalShadow(int index, int splitCount, int tileSize)
    {
        ShadowedDirectionalLight light = shadowedDirctionalLights[index];
        var drawSettings = new ShadowDrawingSettings(cullingResults, light.visibleLightIndex);
        int cascadeCount = settings.directional.cascadeCount;
        int tileOffset = index * cascadeCount;
        Vector3 ratios = settings.directional.CascadeRatios;

        for (int i=0; i < cascadeCount; i++)
        {
            cullingResults.ComputeDirectionalShadowMatricesAndCullingPrimitives(
              light.visibleLightIndex,
              0,
              1,
              Vector3.zero,
              tileSize,
              light.nearPlaneOffset,        
              out Matrix4x4 viewMatrix,
              out Matrix4x4 projectionMatrix,
              out ShadowSplitData shadowSplitData
              );
            drawSettings.splitData = shadowSplitData;
            if(index == 0)
            {
             
                Vector4 cullingSphere = shadowSplitData.cullingSphere;
                /*
                 cullingSphere.w *= cullingSphere.w;
                 cascadeCullingSpheres[i] = cullingSphere;
                 */
                SetCascadeData(index, cullingSphere, tileSize);
            }

            int tileIndex = tileOffset + i;
            Vector2 offset = this.SetTileViewport(tileIndex, splitCount, tileSize);
            dirShadowMatrices[tileIndex] = ConvertoAtlasMatrix(projectionMatrix * viewMatrix, offset, splitCount);
            buffer.SetViewProjectionMatrices(viewMatrix, projectionMatrix);

            //  buffer.SetGlobalDepthBias(50000f, 0f);
            // buffer.SetGlobalDepthBias(0f, 3f);
            buffer.SetGlobalDepthBias(0f, light.slopeScaleBias);
            ExecuteBuffer();
            context.DrawShadows(ref drawSettings);
            buffer.SetGlobalDepthBias(0f, 0f);
        }

      
    }
    Matrix4x4 ConvertoAtlasMatrix(Matrix4x4 m,Vector2 offset,int splitCount)
    {
        if(SystemInfo.usesReversedZBuffer)
        {
            m.m20 = -m.m20;
            m.m21 = -m.m21;
            m.m22 = -m.m22;
            m.m23 = -m.m23;
        }

        float scale = 1f / splitCount;
        m.m00 = (0.5f * (m.m00 + m.m30) + offset.x * m.m30) *scale;
        m.m01 = (0.5f * (m.m01 + m.m31) + offset.x * m.m31) *scale;
        m.m02 = (0.5f * (m.m02 + m.m32) + offset.x * m.m32) *scale;
        m.m03 = (0.5f * (m.m03 + m.m33) + offset.x * m.m33) *scale;

        m.m10 = (0.5f * (m.m10 + m.m30) + offset.x * m.m30) *scale;
        m.m11 = (0.5f * (m.m11 + m.m31) + offset.x * m.m31) *scale;
        m.m12 = (0.5f * (m.m12 + m.m32) + offset.x * m.m32) *scale;
        m.m13 = (0.5f * (m.m13 + m.m33) + offset.x * m.m33) *scale;

        m.m20 = 0.5f * (m.m20 + m.m30);
        m.m21 = 0.5f * (m.m21 + m.m31);
        m.m22 = 0.5f * (m.m22 + m.m32);
        m.m23 = 0.5f * (m.m23 + m.m33);
      
        return m;
    }
    Vector2 SetTileViewport(int index,int splitCount, float tileSize)
    {
        Vector2 offset = new Vector2(index % splitCount, index / splitCount);
        buffer.SetViewport(new Rect(offset.x * tileSize, offset.y * tileSize, tileSize, tileSize));
        return offset;
    }
   

    public void Cleanup()
    {
        if(shadowedDirectionalLightCount > 0 )
        {
            buffer.ReleaseTemporaryRT(dirShadowAtlasId);
            ExecuteBuffer();
        }
    }

    void SetDirectionalKeywords()
    {
        int enbaleIndex = (int)settings.directional.filter - 1;

        for(int i=0; i < directionalFilterKeywords.Length;i++)
        {
            if(i == enbaleIndex)
            {
                buffer.EnableShaderKeyword(directionalFilterKeywords[i]);
            }
            else
            {
                buffer.DisableShaderKeyword(directionalFilterKeywords[i]);
            }
        }
    }

    void SetKeywords(string[] keywords,int index)
    {
        for (int i = 0; i < keywords.Length; i++)
        {
            if (i == index)
            {
                buffer.EnableShaderKeyword(keywords[i]);
            }
            else
            {
                buffer.DisableShaderKeyword(keywords[i]);
            }
        }
    }
   
}
