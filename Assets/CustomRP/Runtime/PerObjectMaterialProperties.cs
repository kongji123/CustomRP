using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Rendering;
using UnityEditor;

[DisallowMultipleComponent]
public class PerObjectMaterialProperties : MonoBehaviour
{
    // Start is called before the first frame update
    static int baseColorId = Shader.PropertyToID("_BaseColor");
    static int cutoffId = Shader.PropertyToID("_Cutoff");
    //static int _ClippingId = Shader.PropertyToID("_Clipping");
    static int _MetallicId = Shader.PropertyToID("_Metallic");
    static int _Smoothness = Shader.PropertyToID("_Smoothness");
    static int _EmissionColorId = Shader.PropertyToID("_EmissionColor");


    [SerializeField]
    Color baseColor = Color.white;
    [SerializeField,Range(0f,1f)]
    float cutoff = 0.5f;

    //[SerializeField]
    //float _Clipping = 0f;

    [SerializeField, Range(0f, 1f)]
    float metallic = 0f, smoothness = 0.5f;

    [SerializeField, ColorUsage(false, true)]
    Color emissionColor = Color.black;

    static MaterialPropertyBlock block;
   
    private void OnValidate()
    {
        if(block == null)
        {
            block = new MaterialPropertyBlock();
        }
        block.SetColor(baseColorId, baseColor);
        block.SetFloat(cutoffId, cutoff);
        //经过测试宏属性不能改，他是编译期开关
      //   block.SetFloat(_ClippingId, _Clipping);
        block.SetFloat(_MetallicId, metallic);
        block.SetFloat(_Smoothness, smoothness);
        block.SetColor(_EmissionColorId, emissionColor);

        GetComponent<Renderer>().SetPropertyBlock(block);
    }

    private void Awake()
    {
        OnValidate();
    }

}
