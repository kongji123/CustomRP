using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CustomRenderPipeline : RenderPipeline
{
    CameraRender cameraRenderer = new CameraRender();
    bool useDynamicBatching, userGPUInstancing;
    ShadowSettings shadowSettings;
    PostFXSettings postFXSettings;
    bool allowHDR;
    int colorLUTResolution;

    public CustomRenderPipeline(bool useDynamicBatching, 
        bool userGPUInstancing,
        bool useSRPBatcher,
        ShadowSettings shadowSettings,
        PostFXSettings postFXSettings,
        bool allowHDR,
        int colorLUTResolution
        )
    {
        this.useDynamicBatching = useDynamicBatching;
        this.userGPUInstancing = userGPUInstancing;
        GraphicsSettings.useScriptableRenderPipelineBatching = useSRPBatcher;
        GraphicsSettings.lightsUseLinearIntensity = true;
        this.shadowSettings = shadowSettings;
        this.postFXSettings = postFXSettings;
        this.allowHDR = allowHDR;
        this.colorLUTResolution = colorLUTResolution;
    }
    protected override void Render(ScriptableRenderContext context, Camera[] cameras)
    {
        foreach(Camera camera in cameras)
        {
            cameraRenderer.Render(context,
                camera, 
                this.useDynamicBatching,
                this.userGPUInstancing,
                this.shadowSettings,
                this.postFXSettings,
                this.allowHDR,
                this.colorLUTResolution
                );
        }
    }

    
}
