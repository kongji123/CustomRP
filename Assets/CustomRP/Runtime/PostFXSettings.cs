﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Rendering/Custom Post FX Settings")]
public class PostFXSettings : ScriptableObject
{
    [SerializeField]
    Shader shader = default;

    [System.NonSerialized]
    Material material;

    public Material Material
    {
        get
        {
            if(material == null && shader != null)
            {
                material = new Material(shader);
                material.hideFlags = HideFlags.HideAndDontSave;
            }
            return material;
        }
    }


    [System.Serializable]
    public struct BloomSettings
    {
        [Range(0, 16f)]
        public int maxIterations;

        [Min(1f)]
        public int downscaleLimit;

        public bool bicubicUpsampling;

        [Min(0f)]
        public float threshold;

        [Range(0f, 1f)]
        public float thresholdKnee;

        [Min(0f)]
        public float intensity;

        public bool fadeFireflies;

        public enum Mode {  Additive, Scattering}

        public Mode mode;

        [Range(0.05f, 0.95f)]
        public float scatter ;
    }

    [SerializeField]
    BloomSettings bloom = new BloomSettings
    {
        scatter = 0.7f
    };

    public BloomSettings Bloom => bloom;



    [Serializable]
    public struct ColorAdjustmentsSettings
    {
        public float postExposure;

        [Range(-100f, 100f)]
        public float constrast;


        [ColorUsage(false, true)]
        public Color colorFilter;

        [Range(-180f, 180f)]
        public float hueShift;

        [Range(-100f, 100f)]
        public float saturation;

    }

    [SerializeField]
    ColorAdjustmentsSettings colorAdjustments = new ColorAdjustmentsSettings
    {
        colorFilter = Color.white
    };
    public ColorAdjustmentsSettings ColorAdjustments => colorAdjustments;

    [Serializable]
    public struct WhiteBalanceSettings
    {
        [Range(-100f,100f)]
        public float temperature, tint;
    }

    [SerializeField]
    WhiteBalanceSettings whiteBalance = default;

    public WhiteBalanceSettings WhiteBalance => whiteBalance;



    [Serializable]
    public struct SplitToningSettings
    {
        [ColorUsage(false)]
        public Color shadows, highlights;

        [Range(-100f, 100f)]
        public float balance;
    }
    [SerializeField]
    SplitToningSettings splitToning = new SplitToningSettings
    {
        shadows = Color.gray,
        highlights = Color.gray
    };
    public SplitToningSettings SplitToning => splitToning;


    [System.Serializable]
    public struct ChannelMixerSettings
    {
        public Vector3 red, green, blue;
    }

    [SerializeField]
    ChannelMixerSettings channelMixer = new ChannelMixerSettings
    {
        red = Vector3.right,
        green = Vector3.up,
        blue = Vector3.forward
    };
    public ChannelMixerSettings ChannelMixer => channelMixer;


    [System.Serializable]
    public struct ShadowsMidtonesHightlightsSettings
    {
        [ColorUsage(false, true)]
        public Color shadows, midtones, hightlights;
        [Range(0f, 2f)]
        public float shadowStart, shadowsEnd, hightlightsStart, hightlightsEnd;
    }
    [SerializeField]
    ShadowsMidtonesHightlightsSettings shadowsMidtonesHightlightsSettings = new ShadowsMidtonesHightlightsSettings
    {
        shadows = Color.white,
        midtones = Color.white,
        hightlights = Color.white,
        shadowsEnd = 0.3f,
        hightlightsStart = 0.55f,
        hightlightsEnd = 1f,
    };
    public ShadowsMidtonesHightlightsSettings ShadowsMidtonesHightlights => shadowsMidtonesHightlightsSettings;





    //色调映射
    [System.Serializable]
    public struct ToneMappingSettings
    {
        public enum Mode
        {
            //None = -1,
            None,
            Reinhard,
            Neutral,
            ACES,
        }
        public Mode mode;
    }

    [SerializeField]
    ToneMappingSettings toneMapping = default;

    public ToneMappingSettings ToneMapping => toneMapping;

}
