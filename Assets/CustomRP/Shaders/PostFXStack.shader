Shader "Hidder/CustomRP/Post FX Stack"
{
   
  
    SubShader
    {
        Cull Off 
        ZTest Always 
        ZWrite Off

        HLSLINCLUDE
        #include "../ShaderLibrary/Common.hlsl"
        #include "./PostFXStackPasses.hlsl"
        ENDHLSL

        Pass {
           
            Name "Copy"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment CopyPassFragment
            
            ENDHLSL
        }

         Pass {
           
            Name "Bloom Horizontal"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomHorizontalPassFragment
            
            ENDHLSL
        }

        
         Pass {
           
            Name "Bloom Vertical"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomVerticalPassFragment
            
            ENDHLSL
        }

        Pass {
           
            Name "Bloom CombineAdd"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomCombineAddPassFragment
            
            ENDHLSL
        }
        Pass {
           
            Name "Bloom Prefilter"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomPrefilterPassFragment
            
            ENDHLSL
        }
       
         Pass {
           
            Name "Bloom PrefilterFireflies"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomPrefilterFirefiesPassFragment
            
            ENDHLSL
        }
       
          Pass {
           
            Name "Bloom CombineScatter"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomCombineScatterPassFragment
            
            ENDHLSL
        }

        Pass {
           
            Name "Bloom CombineScatterFinal"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment BloomScatterFinalPassFragment
            
            ENDHLSL
        }

        Pass {
           
            Name "ToneMappingNone"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ToneMappingNonePassFragment
            
            ENDHLSL
        }

        Pass {
           
            Name "ToneMappingReinhard"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ToneMappingReinhardPassFragment
            
            ENDHLSL
        }
         Pass {
           
            Name "ToneMappingNeutral"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ToneMappingNeutralPassFragment
            
            ENDHLSL
        }
         Pass {
           
            Name "ToneMappingACES"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ToneMappingACESPassFragment
            
            ENDHLSL
        }

          Pass {
           
            Name "ColorGradingNone"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ColorGradingNonePassFragment
            
            ENDHLSL
        }

        Pass {
           
            Name "ColorGradingReinhard"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ColorGradingReinhardPassFragment
            
            ENDHLSL
        }
         Pass {
           
            Name "ColorGradingNeutral"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ColorGradingNeutralPassFragment
            
            ENDHLSL
        }
         Pass {
           
            Name "ColorGradingACES"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment ColorGradingACESPassFragment
            
            ENDHLSL
        }


          Pass {
           
            Name "Final"

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex DefaultPassVertex
            #pragma fragment FinalPassFragment
            
            ENDHLSL
        }
    }
    

}
