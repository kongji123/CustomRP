#ifndef CUSTOM_LIT_INPUT_INCLUDED
#define CUSTOM_LIT_INPUT_INCLUDED

TEXTURE2D(_BaseMap);
TEXTURE2D(_EmissionMap);
TEXTURE2D(_MaskMap);
TEXTURE2D(_DetailMap);
TEXTURE2D(_NormalMap);
TEXTURE2D(_DetailNormalMap);
SAMPLER(sampler_BaseMap);
SAMPLER(sampler_DetailMap);

UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)	
	UNITY_DEFINE_INSTANCED_PROP(float4,_BaseMap_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4,_DetailMap_ST)
	UNITY_DEFINE_INSTANCED_PROP(float4,_BaseColor)
	UNITY_DEFINE_INSTANCED_PROP(float4,_EmissionColor)
	UNITY_DEFINE_INSTANCED_PROP(float,_Cutoff)
	UNITY_DEFINE_INSTANCED_PROP(float,_Metallic)
	UNITY_DEFINE_INSTANCED_PROP(float,_Smoothness)
	UNITY_DEFINE_INSTANCED_PROP(float,_Occlusion)
	UNITY_DEFINE_INSTANCED_PROP(float,_Fresnel)
	UNITY_DEFINE_INSTANCED_PROP(float,_DetailAlbedo)	
	UNITY_DEFINE_INSTANCED_PROP(float,_DetailSmoothness)	
	UNITY_DEFINE_INSTANCED_PROP(float,_NormalScale)	
	UNITY_DEFINE_INSTANCED_PROP(float,_DetailNormalScale)	
UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

#define INSTANCED_INPUT_PROP(name) UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial,name)

float2 TransformBaseUV(float2 baseUV)
{
	float4 baseST = INSTANCED_INPUT_PROP(_BaseMap_ST);
	return baseUV * baseST.xy + baseST.zw;
}
float2 TransformDetailUV(float2 detailUV)
{
	float4 baseST = INSTANCED_INPUT_PROP(_DetailMap_ST);
	return detailUV * baseST.xy + baseST.zw;
}
float4 GetMask(float2 baseUV)
{
	float4 map = SAMPLE_TEXTURE2D(_MaskMap,sampler_BaseMap,baseUV);
	return map;
}

float3 GetNormalTS(float2 baseUV,float2 detailUV = 0.0)
{
	float4 map = SAMPLE_TEXTURE2D(_NormalMap,sampler_BaseMap,baseUV);
	float scale = INSTANCED_INPUT_PROP(_NormalScale);
	float3 normal = DecodeNormal(map,scale);

	float4 map2 = SAMPLE_TEXTURE2D(_DetailNormalMap,sampler_DetailMap,detailUV);
	float scale2 = INSTANCED_INPUT_PROP(_DetailNormalScale) *  GetMask(baseUV).b;
	float3 normal2 = DecodeNormal(map2,scale2);
	normal = BlendNormalRNM(normal,normal2);

	return normal;
}

float4 GetDetail(float2 detailUV)
{
	float4 map = SAMPLE_TEXTURE2D(_DetailMap,sampler_DetailMap,detailUV);
	return map * 2.0 -1.0;
	//return map;
}


float4 GetBase(float2 baseUV,float2 detailUV = 0.0)
{
	float4 map = SAMPLE_TEXTURE2D(_BaseMap,sampler_BaseMap,baseUV);
	float4 color = INSTANCED_INPUT_PROP(_BaseColor);
	//float4 detail = GetDetail(detailUV);
	//map += detail;
	float detail = GetDetail(detailUV).r * INSTANCED_INPUT_PROP(_DetailAlbedo);
	float detailMask = GetMask(baseUV).b;
	map.rgb = lerp(sqrt(map.rgb), detail < 0.0 ? 0.0 : 1.0, abs(detail)* detailMask);
	map.rgb *= map.rgb;//ģ��gamaת��
	
	return map * color;
}

float3 GetEmission(float2 baseUV)
{
	float4 map = SAMPLE_TEXTURE2D(_EmissionMap,sampler_BaseMap,baseUV);
	float4 color = UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial,_EmissionColor);
	return map.rgb * color.rgb;
}

float GetCutoff(float2 baseUV)
{
	return  UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial,_Cutoff);
}



float GetMetallic(float2 baseUV)
{
	float metallic =   INSTANCED_INPUT_PROP(_Metallic);
	metallic *= GetMask(baseUV).r; 
	return metallic;
}

float GetSmoothness(float2 baseUV,float2 detailUV = 0.0 )
{
	float smoothness =   INSTANCED_INPUT_PROP(_Smoothness);
	smoothness *= GetMask(baseUV).a; 

	float detail = GetDetail(detailUV).b *  INSTANCED_INPUT_PROP(_DetailSmoothness);
	float detailMask = GetMask(baseUV).b; 
	smoothness = lerp(smoothness,detail < 0.0 ? 0 : 1.0 ,abs(detail) * detailMask);

	return smoothness;
}

float GetFresnel(float2 baseUV)
{
	return  UNITY_ACCESS_INSTANCED_PROP(UnityPerMaterial,_Fresnel);
}

float GetOcclusion(float2 baseUV)
{
	float occlusion =  GetMask(baseUV).g;
	float factor =   INSTANCED_INPUT_PROP(_Occlusion);
	occlusion = lerp(occlusion,1.0,factor);
	return occlusion;
}

#endif