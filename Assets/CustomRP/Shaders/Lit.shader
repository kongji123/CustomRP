Shader "CustomRP/Lit"
{
   
    Properties
    {
       _BaseColor("Color",Color) = (1,1,1,1)
       _BaseMap("Texture",2D) = "white" {}
       [HideInInspector] _MainTex("Texture for Lightmap",2D) = "white" {}
       [HideInInspector] _Color("Color for Lightmap",Color) = (0.5,0.5,0.5,1.0)

       [Toggle(_CLIPPING)] _Clipping("Alpha Clipping",Float) = 0
       _Cutoff("Alpha Cutoff",Range(0.0,1.0)) = 0.5
       [KeywordEnum(On,Clip,Dither,Off)] _Shadows("Shadows",Float) = 0

       _Metallic("Metallic",Range(0,1)) = 0
       _Smoothness("Smoothness",Range(0,1)) = 0.5  
       _Fresnel("Fresnel",Range(0,1)) = 1
       _Occlusion("Occlusion",Range(0,1)) = 1
       [NoScaleOffset] _MaskMap("Mask (MODS)",2D) = "white" {}
       _DetailMap("Details",2D) = "linearGrey" {}
       _DetailAlbedo("Detail Albedo",Range(0,1)) = 1
       _DetailSmoothness("Detail Smoothness",Range(0,1)) = 1

       [Toggle(_NORMAL_MAP)] _NormalMapToggle("Normal Map",Float) = 0
       [NoScaleOffset] _NormalMap("Normals",2D) = "bump" {}
       _NormalScale("Normal Scale",Range(0,1)) = 1

       [NoScaleOffset] _DetailNormalMap("Detail Normals",2D) = "bump" {}
       _DetailNormalScale("Detail Normal Scale",Range(0,1)) = 1

       [NoScaleOffset] _EmissionMap("Emission",2D) = "white" {}
       [HDR] _EmissionColor("Emission",Color) = (0.0,0.0,0.0,0.0)

        [Toggle(_DIFFUSE_PREMULTIPLY_ALPHA)] _PremultiplyAlpha("Diffuse Premultiply alpha",Float) = 0
       

       [Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("Src Blend",Float) = 1
       [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("Dst Blend",Float) = 0
       [Enum(Off, 0, On, 1] _ZWrite("Z Write",Float) = 1
    }
    SubShader
    {
        HLSLINCLUDE
        #include "../ShaderLibrary/Common.hlsl"
        #include "./LitInput.hlsl"
        ENDHLSL
       
        Pass {
            Tags{
                 "LightMode" = "Meta"
            }
            Cull Off

            
            HLSLPROGRAM

            #pragma target 3.5
            #pragma vertex MetaPassVertex
            #pragma fragment MetaPassFragment
            #include "./MetaPass.hlsl"
            ENDHLSL
        }

        Pass
        {
            Blend [_SrcBlend] [_DstBlend]
            ZWrite [_ZWrite]
            Tags{
               "LightMode" = "CustomLit"
              
            }

            HLSLPROGRAM

            #pragma target 3.5
            #pragma shader_feature _CLIPPING
            #pragma shader_feature _DIFFUSE_PREMULTIPLY_ALPHA
            #pragma shader_feature _NORMAL_MAP
            #pragma multi_compile _ _DIRECTIONAL_PCF3 _DIRECTIONAL_PCF5 _DIRECTIONAL_PCF7  
            #pragma multi_compile _ LIGHTMAP_ON 
            #pragma multi_compile _ _SHADOW_MASK_ALWAYS _SHADOW_MASK_DISTANCE 
            

            #pragma multi_compile_instancing

            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            #include "./LitPass.hlsl"
            ENDHLSL
        }

        Pass 
        {
            Tags {
                "LightMode" = "ShadowCaster"
            }
            ColorMask 0 
              HLSLPROGRAM

            #pragma target 3.5
            //#pragma shader_feature _CLIPPING
            #pragma shader_feature _ _SHADOWS_CLIP _SHADOWS_DITHER
            #pragma multi_compile_instancing

            #pragma vertex ShadowCasterPassVertex
            #pragma fragment ShadowCasterPassFragment
            #include "./ShadowCasterPass.hlsl"
            ENDHLSL
        }
    }
     CustomEditor "CustomShaderGUI"

}
